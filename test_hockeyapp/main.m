//
//  main.m
//  test_hockeyapp
//
//  Created by Paul Perekhozhikh on 26.11.13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ppAppDelegate.h"

int main(int argc, char * argv[])
{
   @autoreleasepool {
       return UIApplicationMain(argc, argv, nil, NSStringFromClass([ppAppDelegate class]));
   }
}
