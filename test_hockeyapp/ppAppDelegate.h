//
//  ppAppDelegate.h
//  test_hockeyapp
//
//  Created by Paul Perekhozhikh on 26.11.13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ppAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
