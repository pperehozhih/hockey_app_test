//
//  ppViewController.m
//  test_hockeyapp
//
//  Created by Paul Perekhozhikh on 26.11.13.
//  Copyright (c) 2013 Paul. All rights reserved.
//

#import "ppViewController.h"

@interface ppViewController ()

@end

@implementation ppViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
   [__label setText:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
